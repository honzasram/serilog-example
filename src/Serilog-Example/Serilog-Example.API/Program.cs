using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using System;
using Serilog;
using Serilog.Events;

namespace Serilog_Example.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            /**
             * This is only for following code.
             * If you use UseSerilog extension method on Host buidder, it will be overwritten by default.
             */
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
                .Enrich.FromLogContext()
                .WriteTo.Console(outputTemplate: "[{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz}] [{MachineName}] [{EnvironmentUserName}] [{Level:u3}] [{SourceContext:l}] {Message:lj}{NewLine}{Exception}")
                .CreateBootstrapLogger();

            try
            {
                /**
                 * One and only instance of correct usage of static Logger instance.
                 * Other should be injected by DI container with correct generic type.
                 */
                Log.Information("Staring!");
                CreateHostBuilder(args).Build().Run();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Host terminated unexpectedly");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                // Here you are setting defaults for logging. Once you call this, it is half job done.
                // It will take standart configuration, however not from appsettings.json and its env. variants.
                // .UseSerilog()
                // for appsetting.json configuration you have to do following.
                .UseSerilog((hostBuilder, serviceProvider, loggerConfig) => {
                    loggerConfig.ReadFrom.Configuration(hostBuilder.Configuration);
                })
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
